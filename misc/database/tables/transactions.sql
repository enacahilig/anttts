-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 09:46 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isko`
--

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `balance` decimal(12,2) NOT NULL,
  `date_created` datetime NOT NULL,
  `type` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `data`, `amount`, `balance`, `date_created`, `type`) VALUES
(1, 1, '', '-1000.00', '10000.00', '2017-09-15 10:57:00', 'widthdrawal'),
(3, 1, '', '-1000.00', '9000.00', '2017-09-15 10:58:02', 'widthdrawal'),
(4, 1, '', '-1000.00', '8000.00', '2017-09-15 11:16:03', 'widthdrawal'),
(5, 1, '', '-1000.00', '7000.00', '2017-09-15 11:16:22', 'widthdrawal'),
(6, 1, '', '-1000.00', '6000.00', '2017-09-15 11:20:41', 'widthdrawal'),
(7, 1, '', '-1500.00', '4500.00', '2017-09-15 11:26:02', 'widthdrawal'),
(8, 1, '', '5000.00', '0.00', '2017-09-16 02:00:00', 'donation'),
(9, 3, '{"amount":"200","cancelled":0,"pledge_id":"1"}', '0.00', '0.00', '0000-00-00 00:00:00', 'pledge'),
(10, 3, '{"amount":"500","cancelled":0,"pledge_id":"2"}', '0.00', '4500.00', '0000-00-00 00:00:00', 'pledge'),
(11, 1, '', '-100.00', '7500.00', '2017-09-15 21:43:11', 'widthdrawal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
