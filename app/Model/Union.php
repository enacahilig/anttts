<?php
App::uses('AppModel', 'Model');


class Union extends AppModel {

	public $useTable = false;

	public function transfer_fund($params = array()){

		$post_fields = $params;

		// $post_fields = array(
		// 	"channel_id" => "isko",
		// 	"transaction_id" => "3-".time(),
		// 	"source_account" => "101150849836",
		// 	"source_currency" => "PHP",
		// 	"target_account" => "100384266652",
		// 	"target_currency" => "PHP",
		// 	"amount" => 1500
		// 	);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api-uat.unionbankph.com/hackathon/sb/transfers/initiate",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($post_fields),
		  // CURLOPT_POSTFIELDS => "{\"channel_id\":\"test3\",\"transaction_id\":\"3\",\"source_account\":\"101150849836\",\"source_currency\":\"PHP\",\"target_account\":\"100384266652\",\"target_currency\":\"PHP\",\"amount\":1500}",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "content-type: application/json",
		    "x-ibm-client-id: bcf57e9b-048f-4a63-9699-26a36ef02a96",
		    "x-ibm-client-secret: J5fJ3cY8vM1eW0mU1cD7cS1kN6rR3qE6tN5cH2qB5dE2wX8oG2"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return array(
				'success' => 0
				);
			// echo "cURL Error #:" . $err;
		} else {
			return array(
				'success' => 1,
				'response' => json_decode($response, true),
				);
			// echo $response;
		}

	}

	public function get_account_details($account_no='')
	{
		$c_id = Configure::read('union.client_id');
		$client_secret = Configure::read('union.client_secret');

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api-uat.unionbankph.com/hackathon/sb/accounts/{$account_no}",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
					"accept: application/json",
					"x-ibm-client-id: bcf57e9b-048f-4a63-9699-26a36ef02a96",
					"x-ibm-client-secret: J5fJ3cY8vM1eW0mU1cD7cS1kN6rR3qE6tN5cH2qB5dE2wX8oG2"
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return array(
				'success' => 0
				);
			// echo "cURL Error #:" . $err;
		} else {
			return array(
				'success' => 1,
				'response' => json_decode($response, true),
				);
			// echo $response;
		}
	}

}
