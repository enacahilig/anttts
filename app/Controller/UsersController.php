<?php
App::uses('AppController', 'Controller');


class UsersController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow('login', 'index', 'temp');
	}

	public function login(){

		$this->layout="login";
		if($this->request->is('post')){
			$class='';
			$email = $this->request->data['email'];
			$password = $this->request->data['password'];
			$log_in_as = $this->request->data['log_in_as'];

			if($user = $this->User->findByEmailAndPassword($email, $password)){
				// pp("User was found");
				$this->Auth->login($user);

				$this->Session->write('User.logged_as', $log_in_as);
				if($log_in_as=="donor"){
					$this->redirect("/users/donor_dashboard");
				}else if($log_in_as=="scholar"){
					$this->redirect("/users/isko_dashboard");	
				}		        
			}else{
				$this->Flash->set('Such a user does not exist!');
				$class = 'alert alert-danger';
			}
			$this->set('user', $user);
		}else{
			$email = '';
			$password = '';
			$class = 'invisible';
		}

		$this->set('email', $email);
		$this->set('password', $password);
		$this->set('class', $class);
	}

	public function isko_dashboard(){
		$this->layout="scholar";

		$logged_user = $this->Auth->user();
		$logged_user_id = $logged_user['User']['id'];
		//first get scholarship of the user if may ara use the start date and end date of scholarship to get the transactions where date created is bewteen the start date and date created of the scholarship
		$this->loadModel('Scholarship'); 
		$scholarship = $this->Scholarship->findByScholarUserId($logged_user_id);
		// print_r($logged_user_id);
		$total_balance = '';
		$redeemable_amount = '';
		if(isset($scholarship)){
			$scholarship_start_date = $scholarship['Scholarship']['start_date'];
			$scholarship_end_date = $scholarship['Scholarship']['end_date'];
			$scholarship_id = $scholarship['Scholarship']['id'];
			$this->loadModel('Transaction');
			$query = "
				SELECT SUM(amount) as total_amount
						FROM transactions
						WHERE user_id='{$logged_user_id}' AND date_created>='{$scholarship_start_date}' AND type='widthdrawal' 
			";
			$transactions = $this->Transaction->query($query);
			
			$transactions_total_amount = $transactions[0][0]['total_amount'];//total_withdrawn
			$total_withdrawn = $transactions_total_amount;
			$scholarship_total_amount = $scholarship['Scholarship']['total_amount'];//total_scholarship
			// print_r($transactions_total_amount);

			// $total_balance = $scholarship_total_amount-$transactions_total_amount;
			$dateToday = date('Y-m-d H:i:s');

			$datetime1 = new DateTime($scholarship_start_date);
			$datetime2 = new DateTime($dateToday);
			$interval = $datetime1->diff($datetime2);

			$start_current = $interval->m; 
			if($interval->m!=0 && $interval->y!=0){
				$start_current = $interval->m + (($interval->y) * 12);
			}else{
				$start_current = $interval->m!=0 ? $interval->m: ($interval->y) * 12 ;
			}

			$datetime3 = new DateTime($scholarship_start_date);
			$datetime4 = new DateTime($scholarship_end_date);
			$interval1 = $datetime3->diff($datetime4);
			
			if($interval1->m!=0 && $interval1->y!=0){
				$start_end = $interval1->m + (($interval1->y) * 12);
			}else{
				$start_end = $interval1->m!=0 ? $interval1->m: ($interval1->y) * 12 ;
			}
			$start_end = 10; // temp
			// print_r($start_current);
		

			$this->loadModel('Pledge');
			$query = "
				SELECT SUM(amount) as total_pledges
						FROM pledges
						WHERE scholarship_id>='{$scholarship_id}' 
			";
			$pledge = $this->Pledge->query($query);
			$total_pledges = isset($pledge) ? $pledge[0][0]['total_pledges']:0;
			// print_r($total_pledges);
		
			$query = "
				SELECT SUM(amount) as total_donation
						FROM transactions
						WHERE user_id='{$logged_user_id}' AND date_created>='{$scholarship_start_date}' AND type='donation' 
			";
			$transactions_donation = $this->Transaction->query($query);
			$total_donation = isset($transactions_donation) ? $transactions_donation[0][0]['total_donation']:0;
			// print_r($total_donation);

			$remaining_amount = $scholarship_total_amount-$total_donation;//
			$remaining_months =  $start_end-$start_current;

			$monthly_amount = $remaining_amount/$remaining_months;

			$fullfillment_percentage = number_format((($total_pledges/$monthly_amount)*100),1);
			// print_r($start_end);
			//start_current/start_end * withdrawable_amount3
		}
		$latest_transaction = $this->get_latest_transaction($logged_user_id);	
		// print_r($latest_transaction);
		$total_balance =  isset($latest_transaction['Transaction']['balance']) ? $latest_transaction['Transaction']['balance']: 0;
		$total_balance = number_format((floor($total_balance)),0);
		$this->set('fullfillment_percentage',$fullfillment_percentage);     
		$this->set('total_balance',$total_balance);

		$redeemable_amount = number_format(floor($scholarship_total_amount*($start_current/$start_end) + $total_withdrawn),0);
		// echo $redeemable_amount;

		$this->set('redeemable_amount',$redeemable_amount<=$total_balance ? ($redeemable_amount>0 ? $redeemable_amount : 0): $total_balance);
	}
	
	public function donor_dashboard(){
		$this->layout="donor";
		$logged_as = $this->Session->read('User.logged_as');

		$this->loadModel('Pledge');
		$logged_user = $this->Auth->user();
		$logged_user_id = $logged_user['User']['id'];
		$logged_user_account_no = $logged_user['User']['account_number'];

		$query = "
				SELECT SUM(amount) as total_pledges
						FROM pledges
						WHERE donor_user_id='{$logged_user_id}' AND cancelled!='1'
			";
		$pledges = $this->Pledge->query($query);
		$total_pledges = isset($pledges) ? $pledges[0][0]['total_pledges']:0;

		// print_r($total_pledges);

		$this->loadModel('Union');
		$union_bank_account_details = $this->Union->get_account_details($logged_user_account_no);
		
		$available_credits = $union_bank_account_details['response'][0]['avaiable_balance'];
		$current_credits = $union_bank_account_details['response'][0]['current_balance'];
		// print_r($current_credits);


		$this->set('available_credits',$available_credits);
		$this->set('current_credits',$current_credits);
		$this->set('total_pledges',$total_pledges);
		// exit;
	}	

	public function logout(){

		$this->Session->destroy();
		$this->Auth->logout();
		$this->redirect("/login");
	}

	public function index(){

	}

	public function search_isko($search_str = ''){
		$this->layout = 'back';
		$this->set('back_link', '/users/donor_dashboard');
		
		$logged_as = $this->Session->read('User.logged_as');
		if($logged_as != "donor"){
			return false;
		}

		if($this->request->is('post') || trim($search_str)!=''){

			if (trim($search_str)!=''){
				$search_value = trim($search_str);
			}else{
				$search_value = $this->request->data['search_isko_value'];
			}
			$this->Session->write('user.search_str', $search_value);
			

			if(isset($search_value) && $search_value!=""){
				$users=$this->_search_scholars($search_value);

				$this->set('users',$users);
			}else{
				$this->Session->setFlash(__('Such a user does not exist!'), 'default', array('class' => 'alert alert-danger'));
			}
			//$class = 'invisible';
		}else{
			$search_value = '';
		}

		$this->set('search_value', $search_value);

		$class= "";
		$this->set('class', $class);
	}

	public function withdraw(){

		$this->layout = 'back';

		$client_id = Configure::read('union.client_id');
		$client_secret = Configure::read('union.client_secret');

		$isko_account_no = Configure::read('isko.account_no');

		$user = $this->Auth->user();
		$user_id = $user['User']['id'];
		$user_account_no = $user['User']['account_number'];
		// print_r($user_id);
		$channel_id = "Isko";
		if($this->request->is('post')){		
			$amount = $this->request->data['amount'];
			// print_r($amount);
			if($amount!=''){
				$latest_transaction = $this->get_latest_transaction($user_id);				

				$prev_balance = isset($latest_transaction['Transaction']['balance']) ? $latest_transaction['Transaction']['balance']: 0;
				// print_r($prev_balance);

				$data= array(
					'user_id' => $user_id,
					'amount' => -1 * $amount,
					'balance' => $prev_balance-$amount ,
					'type' => 'widthdrawal',
					'date_created' => date('Y-m-d H:i:s')
				);
				$this->Transaction->create();
				if($this->Transaction->save($data)){
					$transaction_id = $this->Transaction->id;
					//fund_transfer here
					$this->loadModel("Union");

					$params = array(
						"channel_id" => "isko",
						"transaction_id" => $transaction_id."-".time(),
						"source_account" => $isko_account_no,
						"source_currency" => "PHP",
						"target_account" =>  $user_account_no,
						"target_currency" => "PHP",
						"amount" => $amount
						);
					$transaction_details = $this->Union->transfer_fund($params);
					// print_r($transaction_details);
					if($transaction_details['success']==1){
						$this->redirect('/users/isko_dashboard');
						exit;
						// $this->Flash->set('Success.');
						// $class = 'alert alert-success';
					}else{
						$this->Flash->set($transaction_details['response']['error_message']);
						$class = 'alert alert-danger';
					}
					
				}else{
					$this->Flash->set('Some error occured.');
					$class = 'alert alert-danger';
				}
				// exit;
				//prcess withdrawal from our account to the user acount
				
			}else{
				$this->Flash->set('No amount passed.');
				$class = 'alert alert-danger';
			}
		}else{
			$class = 'invisible';
		}
		$this->set('class', $class);
	}

	public function get_latest_transaction($user_id){
		$this->loadModel('Transaction');
		$conditions = "Transaction.user_id='{$user_id}'";
		$order = "Transaction.id DESC";
	
		$transaction = $this->Transaction->find('first', compact('conditions','order'));

		if($transaction){
			return $transaction;
		}else{
			return false;
		}
	}

	private function _search_scholars($search_str = ''){

		$logged_user = $this->Auth->user();
		$logged_user_id = $logged_user['User']['id'];
		// print_r($logged_user_id);
		$search_str = trim($search_str);
		while (strpos($search_str, '  ')){
			$search_str = str_replace('  ', ' ', $search_str);
		}

		$search_strs = explode(' ', $search_str);
		// print_r($search_strs);

		$cond = '';
		foreach ($search_strs as $str){
			$cond .= $str!='' ? "AND (User.lastname LIKE '{$str}%' OR User.firstname LIKE '{$str}%') " : '';	
		}		

		$today = date('Y-m-d');
		$query = "
			SELECT * 
			FROM `users` User
			WHERE User.id IN (
					SELECT `scholar_user_id`
					FROM scholarships
					WHERE start_date<='{$today}' AND end_date>='{$today}'
			)
			AND User.id != {$logged_user_id}
			{$cond}
			ORDER BY User.lastname, User.firstname
		";
		$users = $this->User->query($query);

		return $users;
	}

	public function temp(){
		$this->loadModel("Union");

		$users = $this->_search_scholars('faj ange');
		print_r($users);

		// $params = array(
		// 	"channel_id" => "isko",
		// 	"transaction_id" => "3-".time(),
		// 	"source_account" => "100384266652",
		// 	"source_currency" => "PHP",
		// 	"target_account" => "101150849836",
		// 	"target_currency" => "PHP",
		// 	"amount" => 1500
		// 	);
		// $transaction_details = $this->Union->transfer_fund($params);		
		// print_r($transaction_details);


		// $account_details = $this->Union->get_account_details(101150849836);
		// print_r($account_details);
		// $account_details = $this->Union->get_account_details(100384266652);
		// print_r($account_details);
		
		exit;
	}

	public function view($id){

		$this->layout="back";
		$search_str = $this->Session->read('user.search_str');
		$this->set("back_link", "/users/search_isko/{$search_str}");

		$user=$this->User->findById($id);
		$this->set("user", $user);
		$pledged = array();
		$scholarship = array();
		if($this->Session->read('User.logged_as') == "donor" &&  $user["User"]["id"] != $this->Auth->user("User.id")){
			//find active scholarship
			$this->loadModel("Scholarship");
			$today = date('Y-m-d');
			$conditions="start_date<='{$today}' AND end_date>='{$today}'
				AND scholar_user_id={$id}";
			$scholarship = $this->Scholarship->find("first", compact("conditions"));
			//check if already pledged to the active scholarship
			$this->loadModel("Pledge");
			$pledged = $this->Pledge->findByScholarshipIdAndDonorUserIdAndCancelled($scholarship["Scholarship"]["id"], $this->Auth->user("User.id"), 0);
		}
		$this->set("pledged", $pledged);
		$this->set("scholarship", $scholarship);
	}

	public function pledge($scholarship_id, $user_id){
		$this->layout="back";
		$this->set("back_link", "/users/view/{$user_id}");
		if($this->request->is('post')){

			$this->loadModel("Pledge");
			$this->Pledge->create();
			$this->Pledge->save( 
				array(
					"scholarship_id"=>$scholarship_id,
					"amount"=>$this->request->data["Pledge"]["amount"],
					"donor_user_id" => $this->Auth->user("User.id"),
					"date_added"=>date("Y-m-d h:i A")
				)
			);
			

			$this->loadModel("Transaction");
			$this->Transaction->create();
			$latest_transaction = $this->get_latest_transaction($user_id);
			$prev_balance = isset($latest_transaction['Transaction']['balance']) ? $latest_transaction['Transaction']['balance']: 0;
			$this->Transaction->save( 
				array(
					"user_id"=>$this->Auth->user("User.id"),
					"amount"=>0,
					"balance"=>$prev_balance,
					"date_created" =>date("Y-m-d h:i A"),
					"type"=>"pledge",
					"data"=>json_encode(
						array(
							"amount"=>$this->request->data["Pledge"]["amount"],
							"cancelled"=>0,
							"pledge_id"=>$this->Pledge->getLastInsertId()
						)
					)
				)
			);

			

			$this->Session->setFlash(__('You have successfully pledge to this scholar.'), 'default', array('class' => 'alert alert-success'));

			$this->redirect("view/{$user_id}");

		}
	}

	public function cancel_pledge($pledge_id, $user_id){
		$this->loadModel("Pledge");
		$this->Pledge->id = $pledge_id;
		$this->Pledge->saveField('cancelled', 1);

		$pledge = $this->Pledge->findById($pledge_id);

		$this->loadModel("Transaction");
		$this->Transaction->create();
		$latest_transaction = $this->get_latest_transaction($user_id);
		$prev_balance = isset($latest_transaction['Transaction']['balance']) ? $latest_transaction['Transaction']['balance']: 0;
		$this->Transaction->save( 
			array(
				"user_id"=>$this->Auth->user("User.id"),
				"amount"=>0,
				"balance"=>$prev_balance,
				"date_created" =>date("Y-m-d h:i A"),
				"type"=>"pledge",
				"data"=>json_encode(
					array(
						"amount"=>$pledge["Pledge"]["amount"],
						"cancelled"=>1,
						"pledge_id"=>$pledge_id
					)
				)
			)
		);

		$this->redirect("view/{$user_id}");
	}

	public function donate($user_id){
		$this->layout="back";
		$this->set("back_link", "/users/view/{$user_id}");	

		if($this->request->is('post')){

			$this->loadModel("Transaction");
			$this->Transaction->create();
			$latest_transaction = $this->get_latest_transaction($user_id);
			$prev_balance = isset($latest_transaction['Transaction']['balance']) ? $latest_transaction['Transaction']['balance']: 0;
			$this->Transaction->save( 
				array(
					"user_id"=>$user_id,
					"amount"=> "+".$this->request->data["User"]["amount"],
					"balance"=>$prev_balance + $this->request->data["User"]["amount"],
					"date_created" =>date("Y-m-d h:i A"),
					"type"=>"donation"
				)
			);

			$this->Session->setFlash(__("You have successfully donated P{$this->request->data["User"]["amount"]} to this scholar."), 'default', array('class' => 'alert alert-success'));

			$this->redirect("view/{$user_id}");

		}
	}
}
?>