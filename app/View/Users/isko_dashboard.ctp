

		<div  id="moneybox">
			<div  id="container"> 

				<div id="top-figure"><div id="figure"  style="color:#c9d3ef"><span  style="color:#c9d3ef">&#8369;&nbsp;</span><?php echo $total_balance;?></div>
				<div id="top-word"  style="color:#c9d3ef">Balance</div>
			
				</div>
				<div id="bottom-two">
					<div  style="color:#c9d3ef"><span id="fulfillment"  style="color:#c9d3ef"><?php echo $fullfillment_percentage;?></span>&nbsp;&#37; <br>Fulfillment (?)</div>
					<a id="withdraw-link" href="<?php echo $this->base;?>/users/withdraw">
						<div style="color: #fff;"><span>&#8369;</span>&nbsp;<span id="balance"  style="font-family: RobotoNormal;"><?php echo $redeemable_amount; ?></span> <br>Redeemable</div>
					</a>

				</div>
			</div>
		
		</div>
		<!-- <div id="notif" class=""> 
			<span>You have unpaid dues : 
			<span>
					<span>&#8369;</span>&nbsp;400
					<a href=""></a>
				</span>
			</span>
			<span class="paybutton">
				PAY
			</span>
	</div> -->

		<div style="font-family: RobotoNormal;font-size: 17px;padding: 5px 10px;">My Activities</div>

		<div id="histbox">
			<div class="hist-item">
				<div>
				<span class="person">Legislators</span>
					<span class="action pled">pledged</span> 
					&nbsp;&#8369;<span class="money">1000</span> 
					</div>

			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>

			</div>
			<div class="hist-item">
				<div>
				<span class="action with">Withdrew</span> 
				&nbsp;&#8369;<span class="money">1000</span> 
				<span class="person"></span>
					</div>
			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>
			</div>
			<div class="hist-item">
				<div>
				<span class="person">Dela Rosa</span>
					<span class="action canc">cancelled pledge</span> 
					&nbsp;&#8369;<span class="money">7000</span> 
					</div>

			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>

			</div>
			
			<div class="hist-item">
				<div>
				<span class="person">US</span>
					<span class="action dona">donated</span> 
					&nbsp;&#8369;<span class="money">730</span>  
					</div>
			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>
			</div>
			<div class="hist-item">
				<div>
				<span class="person">Legislators</span>
					<span class="action pled">pledged</span> 
					&nbsp;&#8369;<span class="money">1000</span> 
					</div>
			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>
			</div>
			<div class="hist-item">
				<div>
				<span class="action with">Withdrew</span> 
				&nbsp;&#8369;<span class="money">1000</span> 
				<span class="person"></span>
					</div>
			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>
			</div>
			<div class="hist-item">
				<div>
				<span class="person">Dela Rosa</span>
					<span class="action canc">cancelled pledge</span> 
					&nbsp;&#8369;<span class="money">7000</span> 
					</div>
			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>
			</div>
			
			<div class="hist-item">
				<div>
				<span class="person">US</span>
					<span class="action dona">donated</span> 
					&nbsp;&#8369;<span class="money">730</span>  
					</div>
			<div class="hist-time">
				<div class="time">10:00 PM</div>
				<div class="date">Sep 12, 2017</div>
			</div>
			</div>
			
			<p style="text-align: center; line-height: 3em; padding: 5px;"><a href="">View All</a>
		</div>




<style type="text/css">


.action.canc{
	/*color: #fd6b6b;*/
	color: #ea4335;
}
.action.with{
	/*color: blue;*/
	color: #4285f4;
}
.action.pled{
	/*color: orange;*/
	color: #fbbc05;
}
.action.dona{
	/*color: green;*/
	color: #34a853;
}

body *{
	font-weight: thin;
	/*background-color: red;*/
}

#moneybox{
	height: 38vh;
	padding: 15px;
	background: linear-gradient(#0E3390,#071a4a);
	/*background: white;*/
	/*box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);*/
	box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
	z-index: 2;
}

#moneybox *{
	color: white;
	line-height: 1.5em;
}

#top-figure, #top-figure span{
	line-height: 1em;
	text-align: center;
	font-size: 20px;
}

#top-figure #figure{
	line-height: 0px;
	padding-top: 60px;
}

#top-figure #figure{
	font-size: 50px;
}

#bottom-two{
	display: flex;
	justify-content: space-around;
}

#bottom-two .word{
	color: rgba(255,255,255,.7);
}
#bottom-two #fulfillment, #bottom-two #balance{
	font-size: 26px;
	opacity: 1;
	color: rgba(255,255,255,.9);
}
#container{
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
}

#top-figure, #bottom-two{
	text-align: center;
}
#top-word{
	font-size: 20px;
}

	display: flex;
	justify-content: space-around;
}

.pr-color{
	background-color: #071a4a;
}
.sc-color{
	background-color: #0E3390;
}
#histbox{
	overflow: auto;
	height: 56vh;
}

#notif{
	padding: 10px;
	line-height: 1em;
	color: white;
	background-color: red;
	display: flex;
	justify-content: space-between;
}

#notif a, #notif span{
	line-height: 1.5em;
	padding: 4px;
	color: white;
}

#notif a, #notif span{
	
}

#notif .paybutton{
	line-height: 1.5em;
	padding: 4px 10px;
	border-radius: 20%;
	border: 1px solid;
}


.hist-item{
	margin-top: 2px;
	font-size: 14px;
	line-height: 1.5em;
	padding: 10px;
	align-items: center;
	background: #fff;
	border-radius: 3px;
	box-shadow: 0 1px 3px 0 rgba(0,0,0,0.15);
	display: flex;
	justify-content: space-between;
}
.hist-item *{
	font-family: RobotoNormal;
}

.hist-item .hist-time .time{
	font-size: 14px;
}

.hist-item .hist-time .date{
	line-height: 1em;
	font-size: 10px;
}

.hist-item:hover{
	box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
}

.hist-item .money, .hist-item .person{
	font-weight: normal;
}

::-webkit-scrollbar {
	width: 4px;
	height: 8px;
	background-color: white; /* or add it to the track */
}
/* Add a thumb */
::-webkit-scrollbar-thumb {
	width: 4px;
	background: rgba(100,100,100,.2); 
}

a#withdraw-link:focus{
	text-decoration: none;
}

</style>