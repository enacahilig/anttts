<div class="clearfix"></div>
<div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
      <br>
        <div >
          <?php echo $this->Form->create("User");?> 
              <div style="font-size: 22px;text-align: center;">Donate</div>
              <br>
              <div class="form-group">
                <?php echo $this->Form->text("amount", array("class"=>"form-control",  "required"=>true));?>
              </div>
              <button type="submit" class="btn btn-lg btn-primary btn-block">Donate</button>
          </div>
         <?php echo $this->Form->end();?>
      </div> 
    </div>
</div>