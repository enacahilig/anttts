
	<div  id="moneybox">
		<div  id="container">
			<a id="withdraw-link" href="<?php echo $this->base;?>/users/withdraw">
				<div id="top-figure">
					<div id="top-word"  style="color:#c9d3ef">Balance :&nbsp;</div>
					<div id="figure"  style="color:#c9d3ef">
						<span  style="color:#c9d3ef">&#8369;</span>
						<span id="balance">1200</span>
					</div>
				</div>
			</a>
		</div>

		<div id="bottom-two">
			<!-- c9d3ef -->
			<div style="color: #fff;">Total Pledge : <span>&#8369;</span>
				<span id="balance"  style="font-family: RobotoNormal;">900</span>
			</div>
		</div>
	</div>
		
		<div id="notif" class=""> 
			<span>You have unpaid dues : 
			<span>
					<span>&#8369;</span>&nbsp;400
					<a href=""></a>
				</span>
			</span>
			<span class="paybutton">
				PAY
			</span>
		</div>

		<div id="title" style="
			    line-height: 1.5em;
			    padding: 10px;
			    font-family: 'RobotoNormal';
			    background: white;
			    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
		">
			My Scholars
		</div>

<style type="text/css">
	.donor-item{
	margin-top: 2px;
	font-size: 14px;
	line-height: 1.5em;
	padding: 10px;
	align-items: center;
	background: #fff;
	border-radius: 3px;
	box-shadow: 0 1px 3px 0 rgba(0,0,0,0.15);
	display: flex;
	justify-content: space-between;
}
.donor-item *{
	font-family: RobotoNormal;
}

.donor-msg{
	font-family: RobotoNormal;
	font-size: 12px;
	line-height: 1.5em;
	padding: 0px 10px;
}

.donor-item .donor-pledge {
	text-align: center;
}

.donor-item .donor-pledge .date{
	line-height: 1em;
	font-size: 10px;
}

.donor-item:hover{
	box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
}

.donor-item .money, .donor-item .person{
	font-weight: normal;
}

.name{
	position: absolute;	
	margin-top: -49px;
    /*margin-left: 10px;*/
    width: 80px;
    text-align: center;
    color: white;
}
.face{
	height: 80px;
	width: 80px;
	border-radius: 50%;
}
</style>

		<div id="donorbox">
			<div class="donor-item">
				<div>
					<img class="face" src="<?php echo $this->base;?>/img/1.jpg" alt="">
					<span class="name">J. Rizal</span> 
				</div>

			<div class="donor-msg"> Hi I am a poor but brilliant student. I grew up in a poor family. I need your help. I am worth it.</div>

			<div class="donor-pledge">
				&#8369;&nbsp;<span class="pledge">1000</span><br>Paid 
			</div>
		</div>

		<div id="add-scholar" style="
			    line-height: 1.5em;
			    padding: 10px;
			    font-family: 'RobotoNormal';
			    background: white;
			    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
		">
			Add Scholar
		</div>

<div>
	<p>Available Credits: <?php echo $available_credits;?></p>
	<p>Current Credits: <?php echo $current_credits;?></p>
	<p>Total Pledges: <?php echo $total_pledges;?></p>
</div>


<style type="text/css">

body *{
	font-weight: thin;
	/*background-color: red;*/
}

#moneybox{
	height: 100%;
	height: 82px;
	padding: 15px;
	background: linear-gradient(#900E33,#4a071a);
	/*background: white;*/
	box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
	z-index: 2;
}

#moneybox *{
	color: white;
	line-height: 1.5em;
}
#top-figure{
	display: flex;
	justify-content: flex-end;
}

#top-figure, #top-figure span{
	line-height: 1em;
	text-align: center;
	font-size: 16px;
}

#top-figure #figure{
	line-height: 1.5em;
	/*padding: 10px;	*/
}


#bottom-two{
	padding-top: 3px;
	font-size: 20px;
	display: flex;
	justify-content: center;
}

/*#bottom-two .word{
	color: rgba(255,255,255,.7);
}*/
#bottom-two #fulfillment, #bottom-two #balance{
	/*font-size: 26px;*/
	opacity: 1;
	color: rgba(255,255,255,.9);
}
#container{
	display: flex;
	/*flex-direction: column;*/
	justify-content: flex-end;
}

#top-figure, #bottom-two{
	text-align: center;
}


	display: flex;
	justify-content: space-around;
}





.pr-color{
	background-color: #071a4a;
}
.sc-color{
	background-color: #0E3390;
}
#donorbox{
	overflow: auto;
	height: 62vh;
}

#notif{
	padding: 10px;
	line-height: 1em;
	color: white;
	background-color: red;
	display: flex;
	justify-content: space-between;
}

#notif a, #notif span{
	line-height: 1.5em;
	padding: 4px;
	color: white;
}

#notif a, #notif span{
	
}

#notif .paybutton{
	line-height: 1.5em;
	padding: 4px 10px;
	border-radius: 20%;
	border: 1px solid;
}




::-webkit-scrollbar {
	width: 4px;
	height: 8px;
	background-color: white; /* or add it to the track */
}
/* Add a thumb */
::-webkit-scrollbar-thumb {
	width: 4px;
	background: rgba(100,100,100,.2); 
}

a#withdraw-link:focus{
	text-decoration: none;
}

</style>

