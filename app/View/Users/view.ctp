<div>
  <?php echo $this->Html->image("{$user['User']['id']}.jpg", array("class"=>"img-thumbnail", "style"=>"border-radius: 50%;width:100px;"));?>
  <p> Name: <?php echo $user["User"]["firstname"];?> <?php echo $user["User"]["lastname"];?></p>
    <?php if($logged_as == "donor" && $logged_user["User"]["id"] != $user['User']['id']):?>
      <?php echo $this->Html->link("Donate", "/users/donate/{$user["User"]["id"]}", array("class"=>"btn btn-success", "escape"=>false));?>
      <?php if($pledged):?>
          Amount: <?php echo $pledged["Pledge"]["amount"];?>
         <?php echo $this->Html->link("Cancel", "/users/cancel_pledge/{$pledged["Pledge"]["id"]}/{$user["User"]["id"]}", array("class"=>"btn btn-default", "escape"=>false, "confirm"=>"Are you sure?"));?>

      <?php else:?>
         <?php echo $this->Html->link("Pledge", "/users/pledge/{$scholarship['Scholarship']['id']}/{$user["User"]["id"]}", array("class"=>"btn btn-info", "escape"=>false));?>
      <?php endif;?>

  <?php endif;?>
</div>
