<img src="<?php echo $this->base;?>/img/logo4-black.png" style="
	width: 50%;
	display: block;
	margin: 0 auto;
	margin-top: -10px;
	margin-bottom: 30px;
	">

<form class="col-sm-12 col-md-4 col-md-offset-4" method="post">

	<div class="form-group" style="text-align:center;">
		<label >Log in as:</label>
		&nbsp;&nbsp;&nbsp;
		<label for="as_scholar"><input type="radio" class="" id="as_scholar" name="log_in_as" value="scholar" checked>&nbsp;Scholar</label>
		&nbsp;
		<label for="as_donor"><input type="radio" class="" id="as_donor" name="log_in_as" value="donor">&nbsp;Donor</label>
	</div>

	
	<div class="form-group">
		<label for="exampleInputEmail1">Email address</label>
		<input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" value="<?php echo $email; ?>">
	</div>
	<div class="form-group">
		<label for="exampleInputPassword1">Password</label>
		<input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" value="<?php echo $password; ?>">
	</div>
	
	<div class="<?php echo $class ?>">
		<?php echo $this->Flash->render(); ?>
	</div>

	<div style="text-align:center;">
		<button type="submit" class="btn btn-primary btn-block">Login</button>
	</div>
</form>

