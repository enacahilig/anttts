
<form class="col-sm-12 col-md-4 col-md-offset-4" method="post" action="<?php echo $this->base;?>/users/search_isko">
	<div style="font-size: 22px;text-align: center;">Add Scholar</div>
	<br>
  <div class="form-group">
    <input type="text" class="form-control" id="search_isko" name="search_isko_value" placeholder="Enter keywords" value="<?php echo $search_value;?>">
  </div>
  <button type="submit" class="btn btn-primary">Search</button>
</form>
<br>
<?php if(isset($users) && count($users)>0): ?>
	<?php foreach($users as $user): ?>
			<div style="padding:10px;font-size:16px;">
				<a href="<?php echo $this->base;?>/users/view/<?php echo $user['User']['id']?>">
				<?php echo $this->Html->image("{$user['User']['id']}.jpg", array("class"=>"img-thumbnail", "style"=>"border-radius: 50%;width:50px;"));?>
				<?php echo $user['User']['firstname']?> <?php echo $user['User']['lastname']?> 
			</div>
	<?php endforeach ?>
<?php elseif(isset($users)): ?>
		<div style="padding:10px;font-size:16px;">
			No results found.
		</div>
<?php endif ?>
