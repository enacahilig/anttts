<div class="clearfix"></div>
<div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
      <br>
        <div >
          <?php echo $this->Form->create("Pledge");?> 
              <div style="font-size: 22px;text-align: center;">Monthly Pledge </div>
              <br>
              <div class="form-group">
                <?php echo $this->Form->select("amount",
                    array(
                      "100"=>"P 100.00", 
                      "200"=>"P 200.00", 
                      "300"=>"P 300.00",
                      "400"=>"P 400.00",
                      "500"=>"P 500.00"
                    ),  
                    array("class"=>"form-control",  "required"=>true,"empty"=>false)
                );?>
              </div>
              <button type="submit" class="btn btn-lg btn-primary btn-block">Pledge</button>
          </div>
         <?php echo $this->Form->end();?>
      </div> 
    </div>
</div>