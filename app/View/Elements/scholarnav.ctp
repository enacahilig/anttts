<nav class="navbar navbar-default navbar-fixed-top" style="
	background: transparent;
	border: 0;
	border-radius: 0;
	border-bottom: 7px solid transparent;
	">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					style="
						margin-left: 5px;
						margin-right: 15px;
						float: left;
						border-color: transparent;
					" 
	            >
	                <!-- <span class="sr-only">Toggle navigation</span> -->
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	        </div>

	        <!-- Collect the nav links, forms, and other content for toggling -->
	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  style="background-color: rgba(255,255,255,.7); color: black!important;">
	            <ul class="nav navbar-nav">
	                <li><a style="color: black;"href="<?php echo $this->base;?>/users/logout"> Logout</a></li>
	            </ul>
	            <!-- <ul class="nav navbar-nav navbar-right"></ul> -->
	        </div>
	    </div>
	</nav>

<style type="text/css">
.icon-bar{
	background-color: grey;

}

.navbar-default *{
	/*color: #ffffff !important;*/
}
.navbar-default .navbar-nav>li>a{
	color: black !important;	
}
.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{
	background: rgba(255,255,255,.5);
}
.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{
	background: rgba(255,255,255,.5);
}
.navbar-default .navbar-toggle{
	border-color: #fff;
	/*#fdc358;*/
}
.navbar-default .navbar-toggle .icon-bar{
	background-color: #fff;
	/*#fdc358;	*/
}
.navbar-default .caret{
	color: #fff;
}
.navbar-default .navbar-nav .open .dropdown-menu>li>a{
	color: black;	
}

#bs-example-navbar-collapse-1{
	color: transparent;
}
navbar-collapse.collapse{
	color: transparent;
}
navbar-collapse.collapse.in{
	background-color: transparent;
	border-top: 0px;
}
.collapse navbar-collapse div li a{
	color: white
}
</style>